// Niebieski    - H-(123-125) S-(151-155) L-(70-82)
// Pomaranczowy - H-(26-28) S-(207-213) L-(104-112)
// Zielony      - H-(46-48) S-(231-240) L-(72-75)
// Różowy       - H-(228-230) S-(127-130) L-(140-143)
// Żółty        - H-(38-40) S-(217-226) L-(80-88)

#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <string>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char** argv)
{

    VideoCapture video(0); // przechwycenie kamery id 0 domyślnie

    if (!video.isOpened()) // sprawdzanie połączenia
    {
        cout << "Nie mogę otworzyć kamerki. Sprawdz podłączenie." << endl;
        return -1;
    }

    Mat frame, img, tmp_img, HSV_img, thresh_img;

    namedWindow("DrawIt", CV_WINDOW_AUTOSIZE);
    namedWindow("Colors", CV_WINDOW_AUTOSIZE);

    int MinHue = 37;
    int MaxHue = 120;

    int MinSaturation = 41;
    int MaxSaturation = 246;

    int MinValue = 120;
    int MaxValue = 223;

    createTrackbar("Min Hue", "Colors", &MinHue, 179); // 0 - 180 stopni
    createTrackbar("Max Hue", "Colors", &MaxHue, 179);

    createTrackbar("Min Saturation", "Colors", &MinSaturation, 255); // 0 - 255 zakres
    createTrackbar("Max Saturation", "Colors", &MaxSaturation, 255);

    createTrackbar("Min Value", "Colors", &MinValue, 255); // 0 - 255 zakres
    createTrackbar("Max Value", "Colors", &MaxValue, 255);

    video.read(tmp_img);
    Mat imgDum = Mat::zeros(tmp_img.size(), CV_8UC3); // tworzenie pustego obrazu takiego samego rozmiarowo jak kamera

    int wX = 0;
    int wY = 0;

    while (waitKey(20) != 27) // wychodzenie escape
    {
        if (video.read(frame)) {

            cvtColor(frame, HSV_img, COLOR_BGR2HSV); // zamiana BGR na HSV
            inRange(HSV_img, Scalar(MinHue, MinSaturation, MinValue), Scalar(MaxHue, MaxSaturation, MaxValue), thresh_img); // ekstrakcja zakresu HSV (koloru)

            //metoda usuwania niechcianych obiektów z planu
            erode(thresh_img, thresh_img, getStructuringElement(MORPH_ELLIPSE, Size(5, 5))); // erozja - powiekszanie
            dilate(thresh_img, thresh_img, getStructuringElement(MORPH_ELLIPSE, Size(5, 5))); // dilation - uszczuplanie

            //metoda zakrywania dziur z planu
            dilate(thresh_img, thresh_img, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
            erode(thresh_img, thresh_img, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

            Moments Agar = moments(thresh_img); // wyznaczanie zbioru punktów z wyciętego zakresem HSV obrazu

            double p01 = Agar.m01; //momenty przestrzenne
            double p10 = Agar.m10;
            double p00 = Agar.m00;

            if (p00 > 10000) {
                //obliczanie pozycji przedmiotu
                int posX = p10 / p00;
                int posY = p01 / p00;

                if (wX >= 0 && wY >= 0 && posX >= 0 && posY >= 0) {
                    line(imgDum, Point(posX, posY), Point(wX, wY), Scalar(0, 255, 0), 5);
                }

                wX = posX;
                wY = posY;
            }
            imshow("Obszar", thresh_img);
            frame = frame + imgDum; // nakładanie linii na obraz oryginalny
            imshow("DrawIt", frame);
        }
        else
            break;
    }

    return 0;
}
